package com.valiit.tripapp.util;

public class Helper {

    public static Double round(Double value) {
        return Math.round(value * 100) / 100.0;
    }
}
