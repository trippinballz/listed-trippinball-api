package com.valiit.tripapp.util;

import com.valiit.tripapp.dto.TripDto;
import com.valiit.tripapp.model.Trip;

import java.time.LocalDate;

public class Transformer {

    public static Trip toTripModel(Trip initialObject) {
        if (initialObject == null) {
            return null;
        }
        Trip resultingObject = new Trip();
        resultingObject.setTripID(initialObject.getTripID());
        resultingObject.setUserID(initialObject.getUserID());
        resultingObject.setTripStartTime(initialObject.getTripStartTime());
        resultingObject.setTripEndTime(initialObject.getTripEndTime());
        resultingObject.setTripLocation(initialObject.getTripLocation());
        resultingObject.setTripType(initialObject.getTripType());

        return null;
    }

    public static TripDto toTripDto(Trip initialObject) {
        if (initialObject == null) {
            return null;
        }

//        TripDto resultingObject = new TripDto();
//        resultingObject.setId(initialObject.getId());
//        resultingObject.setName(initialObject.getName());
//        resultingObject.setLogo(initialObject.getLogo());
//        resultingObject.setEstablished(initialObject.getEstablished());
//        resultingObject.setEmployees(initialObject.getEmployees());
//        resultingObject.setRevenue(initialObject.getRevenue());
//        resultingObject.setNetIncome(initialObject.getNetIncome());
//        resultingObject.setSecurities(initialObject.getSecurities());
//        resultingObject.setSecurityPrice(initialObject.getSecurityPrice());
//        resultingObject.setDividends(initialObject.getDividends());
//
//        if (initialObject.getSecurities() > 0 && initialObject.getSecurityPrice() > 0) {
//            Double marketCapitalization = Helper.round(initialObject.getSecurities() * initialObject.getSecurityPrice());
//            resultingObject.setMarketCapitalization(marketCapitalization);
//        }
//
//        if (initialObject.getSecurityPrice() > 0 && initialObject.getDividends() > 0) {
//            Double dividendYield = Helper.round(initialObject.getDividends() / initialObject.getSecurityPrice());
//            resultingObject.setDividendYield(dividendYield);
//        }

        return null;
    }
}
