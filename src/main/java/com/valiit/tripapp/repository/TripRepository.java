package com.valiit.tripapp.repository;

import com.valiit.tripapp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TripRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<Trip> getTrips(int userID) {
        return jdbcTemplate.query("select * from trip where userID = ?", new Object[]{userID}, (row, num) -> {
            Trip trip = new Trip();
            trip.setTripID(row.getInt("tripID"));
            trip.setUserID(row.getInt("userID"));
            trip.setTripStartTime(row.getDate("tripStartTime") != null ?
                    row.getDate("tripStartTime").toLocalDate() : null);
            trip.setTripEndTime(row.getDate("tripEndTime") != null ?
                    row.getDate("tripEndTime").toLocalDate() : null);
            trip.setTripLocation(row.getString("tripLocation"));
            trip.setTripType(row.getString("tripType"));

            trip.setAccomodations(getTripAccommodations(row.getInt("tripID")));
            trip.setChecklists(getTripChecklist(row.getInt("tripID")));
            trip.setTasks(getTaskItem(row.getInt("tripID")));
            trip.setTransport(getTripTransport(row.getInt("tripID")));
            trip.setTravelDocuments(getTripTravelDocuments(row.getInt("tripID")));

            return trip;
        });

    }

    public Trip getTrip(int tripID) {
        List<Trip> trips = jdbcTemplate.query("select * from trip where tripID = ?", new Object[]{tripID}, (row, num) -> {
            Trip trip = new Trip();

            trip.setTripID(row.getInt("tripID"));
            trip.setUserID(row.getInt("userID"));
            trip.setTripStartTime(row.getDate("tripStartTime") != null ?
                    row.getDate("tripStartTime").toLocalDate() : null);
            trip.setTripEndTime(row.getDate("tripEndTime") != null ?
                    row.getDate("tripEndTime").toLocalDate() : null);
            trip.setTripLocation(row.getString("tripLocation"));
            trip.setTripType(row.getString("tripType"));
            trip.setAccomodations(getTripAccommodations(row.getInt("tripID")));
            trip.setChecklists(getTripChecklist(row.getInt("tripID")));
            trip.setTasks(getTaskItem(row.getInt("tripID")));
            trip.setTransport(getTripTransport(row.getInt("tripID")));
            trip.setTravelDocuments(getTripTravelDocuments(row.getInt("tripID")));
            return trip;
        });

        return trips.size() > 0 ? trips.get(0) : null;
    }

//    get all accomodations

    public List<Accomodation> getTripAccommodations(int tripID) {
        return jdbcTemplate.query("select * from accomodation where tripID = ?",
                new Object[]{tripID}, (row, num) -> {
                    Accomodation accomodation = new Accomodation();
                    accomodation.setTripID(row.getInt("tripID"));
                    accomodation.setId(row.getInt("id"));
                    accomodation.setAccomodationType(row.getString("accomodationType"));
                    accomodation.setAccomodationName(row.getString("accomodationName"));
                    accomodation.setAccomodationAddress(row.getString("accomodationAddress"));
                    accomodation.setAccomodationPhoneNo(row.getString("accomodationPhoneNo"));
                    accomodation.setAccomodationStartTime(row.getDate("accomodationStartTime").toLocalDate() != null ?
                            row.getDate("accomodationStartTime").toLocalDate() : null);
                    accomodation.setAccomodationEndTime(row.getTimestamp("accomodationEndTime") != null ?
                            row.getDate("accomodationEndTime").toLocalDate() : null);
                    accomodation.setAccomodationExtraInfo(row.getString("accomodationExtraInfo"));
                    return accomodation;
                });
    }

//    get one accomodation

    public Accomodation getAccomodation(Integer id) {
        List<Accomodation> accomodations = jdbcTemplate.query("select * from accomodation where id = ?",
                new Object[]{id}, (row, num) -> {
                    Accomodation accomodation = new Accomodation();

                    accomodation.setTripID(row.getInt("tripID"));
                    accomodation.setId(row.getInt("id"));
                    accomodation.setAccomodationType(row.getString("accomodationType"));
                    accomodation.setAccomodationName(row.getString("accomodationName"));
                    accomodation.setAccomodationAddress(row.getString("accomodationAddress"));
                    accomodation.setAccomodationPhoneNo(row.getString("accomodationPhoneNo"));
                    accomodation.setAccomodationStartTime(row.getDate("accomodationStartTime").toLocalDate() != null ?
                            row.getDate("accomodationStartTime").toLocalDate() : null);
                    accomodation.setAccomodationEndTime(row.getTimestamp("accomodationEndTime") != null ?
                            row.getDate("accomodationEndTime").toLocalDate() : null);
                    accomodation.setAccomodationExtraInfo(row.getString("accomodationExtraInfo"));
                    return accomodation;

                });

        return accomodations.size() > 0 ? accomodations.get(0) : null;
    }

//    get one transport

    public Transport getTransport(Integer id) {
        List<Transport> transports = jdbcTemplate.query("select * from transport where id = ?",
                new Object[]{id}, (row, num) -> {
                    Transport transport = new Transport();

                    transport.setId(row.getInt("id"));
                    transport.setTripID(row.getInt("tripID"));
                    transport.setTransportType(row.getString("transportType"));
                    transport.setTransportProvider(row.getString("transportProvider"));
                    transport.setTransportStartTime(row.getDate("transportStartTime") != null ?
                            row.getDate("transportStartTime").toLocalDate() : null);
                    transport.setTransportEndTime(row.getDate("transportEndTime") != null ?
                            row.getDate("transportEndTime").toLocalDate() : null);
                    transport.setTransportStartLocation(row.getString("transportStartLocation"));
                    transport.setTransportEndLocation(row.getString("transportEndLocation"));

                    return transport;

                });

        return transports.size() > 0 ? transports.get(0) : null;
    }


    private List<Checklist> getTripChecklist(int tripID) {
        return jdbcTemplate.query("select * from checklist where tripID = ?",
                new Object[]{tripID}, (row, num) -> {
                    Checklist checklist = new Checklist();
                    checklist.setId(row.getInt("id"));
                    checklist.setTripID(row.getInt("tripID"));
                    checklist.setListItem(row.getString("listItem"));
                    return checklist;
                });
    }

    private List<Task> getTaskItem(int tripID) {
        return jdbcTemplate.query("select * from task where tripID = ?",
                new Object[]{tripID}, (row, num) -> {
                    Task task = new Task();
                    task.setId(row.getInt("id"));
                    task.setTripID(row.getInt("tripID"));
                    task.setTaskItem(row.getString("taskItem"));
                    return task;
                });
    }

    // get all transports
    private List<Transport> getTripTransport(int tripID) {
        return jdbcTemplate.query("select * from transport where tripID = ?",
                new Object[]{tripID}, (row, num) -> {
                    Transport transport = new Transport();
                    transport.setId(row.getInt("Id"));
                    transport.setTripID(row.getInt("tripID"));
                    transport.setTransportType(row.getString("transportType"));
                    transport.setTransportProvider(row.getString("transportProvider"));
                    transport.setTransportStartTime(row.getDate("transportStartTime") != null ?
                            row.getDate("transportStartTime").toLocalDate() : null);
                    transport.setTransportEndTime(row.getDate("transportEndTime") != null ?
                            row.getDate("transportEndTime").toLocalDate() : null);
                    transport.setTransportStartLocation(row.getString("transportStartLocation"));
                    transport.setTransportEndLocation(row.getString("transportEndLocation"));

                    return transport;
                });
    }

    // get all travel documents
    private List<TravelDocument> getTripTravelDocuments(int tripID) {
        return jdbcTemplate.query("select * from travelDocuments where tripID = ?",
                new Object[]{tripID}, (row, num) -> {
                    TravelDocument travelDocument = new TravelDocument();
                    travelDocument.setId(row.getInt("id"));
                    travelDocument.setTripID(row.getInt("tripID"));
                    travelDocument.setTravelDocumentName(row.getString("travelDocumentName"));
                    travelDocument.setTravelDocumentStartTime(row.getDate("travelDocumentStartTime") != null ?
                            row.getDate("travelDocumentStartTime").toLocalDate() : null);
                    travelDocument.setTravelDocumentEndTime(row.getDate("travelDocumentEndTime") != null ?
                            row.getDate("travelDocumentEndTime").toLocalDate() : null);
                    travelDocument.setTravelDocumentExtraInfo(row.getString("travelDocumentExtraInfo"));

                    return travelDocument;
                });
    }

    //    get one travelDocument
    public TravelDocument getTravelDocument(Integer id) {
        List<TravelDocument> travelDocuments = jdbcTemplate.query("select * from traveldocuments where id = ?",
                new Object[]{id}, (row, num) -> {

                    TravelDocument travelDocument = new TravelDocument();

                    travelDocument.setId(row.getInt("id"));
                    travelDocument.setTripID(row.getInt("tripID"));
                    travelDocument.setTravelDocumentName(row.getString("travelDocumentName"));
                    travelDocument.setTravelDocumentStartTime(row.getDate("travelDocumentStartTime") != null ?
                            row.getDate("travelDocumentStartTime").toLocalDate() : null);
                    travelDocument.setTravelDocumentEndTime(row.getDate("travelDocumentEndTime") != null ?
                            row.getDate("travelDocumentEndTime").toLocalDate() : null);
                    travelDocument.setTravelDocumentExtraInfo(row.getString("travelDocumentExtraInfo"));

                    return travelDocument;

                });

        return travelDocuments.size() > 0 ? travelDocuments.get(0) : null;
    }


    public void addTrip(Trip trip) {
        jdbcTemplate.update("insert into trip (userID, tripStartTime, tripEndTime, tripLocation, tripType) " +
                        "values (?, ?, ?, ?, ?)",
                trip.getUserID(), trip.getTripStartTime(), trip.getTripEndTime(), trip.getTripLocation(), trip.getTripType());
    }


    public void addAccomodation(Accomodation accomodation) {
        jdbcTemplate.update("insert into accomodation (tripID, accomodationType, accomodationName, " +
                        "accomodationAddress, accomodationPhoneNo, accomodationStartTime," +
                        "accomodationEndTime, accomodationExtraInfo)" +
                        "values (?, ?, ?, ?, ?, ?, ?, ?)",
                accomodation.getTripID(), accomodation.getAccomodationType(), accomodation.getAccomodationName(),
                accomodation.getAccomodationAddress(), accomodation.getAccomodationPhoneNo(),
                accomodation.getAccomodationStartTime(), accomodation.getAccomodationEndTime(),
                accomodation.getAccomodationExtraInfo());
    }

    public void addChecklist(Checklist checklist) {
        jdbcTemplate.update("insert into checklist (tripID, listItem) values (?, ?)",
                checklist.getTripID(), checklist.getListItem());
    }

    public void addTask(Task task) {
        jdbcTemplate.update("insert into task (tripID, taskItem) values (?, ?)",
                task.getTripID(), task.getTaskItem());
    }

    public void addTransport(Transport transport) {
        jdbcTemplate.update("insert into transport (tripID, transportType, transportProvider, " +
                        "transportStartTime, transportEndTime, transportStartLocation, transportEndLocation)" +
                        "values (?, ?, ?, ?, ?, ?, ?)",
                transport.getTripID(), transport.getTransportType(), transport.getTransportProvider(),
                transport.getTransportStartTime(), transport.getTransportEndTime(),
                transport.getTransportStartLocation(), transport.getTransportEndLocation());
    }

    public void addTravelDocument(TravelDocument travelDocument) {
        jdbcTemplate.update("insert into traveldocuments (tripID, travelDocumentName, travelDocumentStartTime, travelDocumentEndTime, " +
                        "travelDocumentExtraInfo)" +
                        "values (?, ?, ?, ?, ?)",
                travelDocument.getTripID(), travelDocument.getTravelDocumentName(), travelDocument.getTravelDocumentStartTime(),
                travelDocument.getTravelDocumentEndTime(), travelDocument.getTravelDocumentExtraInfo());
    }

    // UPDATE METHODS

    public void updateTrip(Trip trip) {
        jdbcTemplate.update("update trip set userID = ?, tripStartTime = ?, tripEndTime = ?, " +
                        "tripLocation = ?, tripType = ? where tripID = ?",
                trip.getUserID(), trip.getTripStartTime(), trip.getTripEndTime(), trip.getTripLocation(),
                trip.getTripType(), trip.getTripID());
    }

    public void updateAccomodation(Accomodation accomodation) {
        jdbcTemplate.update("update accomodation set tripID = ?, accomodationType = ?, accomodationName = ?, " +
                        "accomodationAddress = ?, accomodationPhoneNo = ?, accomodationStartTime = ?," +
                        "accomodationEndTime = ?, accomodationExtraInfo = ? where id = ?", +
                        accomodation.getTripID(), accomodation.getAccomodationType(), accomodation.getAccomodationName(),
                accomodation.getAccomodationAddress(), accomodation.getAccomodationPhoneNo(),
                accomodation.getAccomodationStartTime(), accomodation.getAccomodationEndTime(),
                accomodation.getAccomodationExtraInfo(), accomodation.getId());
    }

    public void updateChecklist(Checklist checklist) {
        jdbcTemplate.update("update checklist set tripID = ?, listItem = ? where id = ?", +
                checklist.getTripID(), checklist.getListItem(), checklist.getId());
    }

    public void updateTask(Task task) {
        jdbcTemplate.update("update task set tripID = ?, taskItem = ?  where id = ?", +
                task.getTripID(), task.getTaskItem(), task.getId());
    }

    public void updateTransport(Transport transport) {
        jdbcTemplate.update("update transport set tripID = ?, transportType = ?, transportProvider = ?, " +
                        "transportStartTime = ?, transportEndTime = ?, transportStartLocation = ?, " +
                        "transportEndLocation = ? where id = ?", +
                        transport.getTripID(), transport.getTransportType(), transport.getTransportProvider(),
                transport.getTransportStartTime(), transport.getTransportEndTime(),
                transport.getTransportStartLocation(), transport.getTransportEndLocation(), transport.getId());
    }

    public void updateTravelDocument(TravelDocument travelDocument) {
        jdbcTemplate.update("update traveldocuments set tripID = ?, travelDocumentName = ?, travelDocumentStartTime = ?, " +
                        "travelDocumentEndTime = ?, travelDocumentExtraInfo = ? where id = ?", +
                        travelDocument.getTripID(), travelDocument.getTravelDocumentName(), travelDocument.getTravelDocumentStartTime(),
                travelDocument.getTravelDocumentEndTime(), travelDocument.getTravelDocumentExtraInfo(),
                travelDocument.getId());
    }

    public boolean tripExists(Trip trip) {
        Integer count = jdbcTemplate.queryForObject("select count(tripID) from trip where tripID = ?", new Object[]{trip.getTripID()}, Integer.class);
        return count != null && count > 0;
    }

    public boolean accomodationExists(Accomodation accomodation) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from accomodation where id = ?",
                new Object[]{accomodation.getId()}, Integer.class);
        return count != null && count > 0;
    }

    public boolean checklistExists(Checklist checklist) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from checklist where id = ?",
                new Object[]{checklist.getId()}, Integer.class);
        return count != null && count > 0;
    }

    public boolean taskExists(Task task) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from task where id = ?",
                new Object[]{task.getId()}, Integer.class);
        return count != null && count > 0;
    }

    public boolean transportExists(Transport transport) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from transport where id = ?",
                new Object[]{transport.getId()}, Integer.class);
        return count != null && count > 0;
    }

    public boolean travelDocumentExists(TravelDocument travelDocument) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from traveldocuments where id = ?",
                new Object[]{travelDocument.getId()}, Integer.class);
        return count != null && count > 0;
    }


    public void deleteTrip(int tripID) {
        jdbcTemplate.update("delete from accomodation where tripID = ?", tripID);
        jdbcTemplate.update("delete from checklist where tripID = ?", tripID);
        jdbcTemplate.update("delete from task where tripID = ?", tripID);
        jdbcTemplate.update("delete from transport where tripID = ?", tripID);
        jdbcTemplate.update("delete from traveldocuments where tripID = ?", tripID);
        jdbcTemplate.update("delete from trip where tripID = ?", tripID);
    }

    public void deleteAccomodation(Integer id) {
        jdbcTemplate.update("delete from accomodation where id = ?", id);
    }

    public void deleteChecklistItem(Integer id) {
        jdbcTemplate.update("delete from checklist where id = ?", id);
    }

    public void deleteTask(Integer id) {
        jdbcTemplate.update("delete from task where id = ?", id);
    }

    public void deleteTransport(Integer id) {
        jdbcTemplate.update("delete from transport where id = ?", id);
    }

    public void deleteTravelDocument(Integer id) {
        jdbcTemplate.update("delete from traveldocuments where id = ?", id);
    }


}

