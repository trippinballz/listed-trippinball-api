package com.valiit.tripapp.model;

import java.time.LocalDate;

public class Transport {
    private Integer id;
    private int tripID;
    private String transportType;
    private String transportProvider;
    private LocalDate transportStartTime;
    private LocalDate transportEndTime;
    private String transportStartLocation;
    private String transportEndLocation;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getTransportProvider() {
        return transportProvider;
    }

    public void setTransportProvider(String transportProvider) {
        this.transportProvider = transportProvider;
    }

    public LocalDate getTransportStartTime() {
        return transportStartTime;
    }

    public void setTransportStartTime(LocalDate transportStartTime) {
        this.transportStartTime = transportStartTime;
    }

    public LocalDate getTransportEndTime() {
        return transportEndTime;
    }

    public void setTransportEndTime(LocalDate transportEndTime) {
        this.transportEndTime = transportEndTime;
    }

    public String getTransportStartLocation() {
        return transportStartLocation;
    }

    public void setTransportStartLocation(String transportStartLocation) {
        this.transportStartLocation = transportStartLocation;
    }

    public String getTransportEndLocation() {
        return transportEndLocation;
    }

    public void setTransportEndLocation(String transportEndLocation) {
        this.transportEndLocation = transportEndLocation;
    }
}
