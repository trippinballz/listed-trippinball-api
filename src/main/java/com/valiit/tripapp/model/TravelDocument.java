package com.valiit.tripapp.model;

import java.time.LocalDate;

public class TravelDocument {

    private Integer id;
    private int tripID;
    private String travelDocumentName;
    private LocalDate travelDocumentStartTime;
    private LocalDate travelDocumentEndTime;
    private String travelDocumentExtraInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public String getTravelDocumentName() {
        return travelDocumentName;
    }

    public void setTravelDocumentName(String travelDocumentName) {
        this.travelDocumentName = travelDocumentName;
    }

    public LocalDate getTravelDocumentStartTime() {
        return travelDocumentStartTime;
    }

    public void setTravelDocumentStartTime(LocalDate travelDocumentStartTime) {
        this.travelDocumentStartTime = travelDocumentStartTime;
    }

    public LocalDate getTravelDocumentEndTime() {
        return travelDocumentEndTime;
    }

    public void setTravelDocumentEndTime(LocalDate travelDocumentEndTime) {
        this.travelDocumentEndTime = travelDocumentEndTime;
    }

    public String getTravelDocumentExtraInfo() {
        return travelDocumentExtraInfo;
    }

    public void setTravelDocumentExtraInfo(String travelDocumentExtraInfo) {
        this.travelDocumentExtraInfo = travelDocumentExtraInfo;
    }

}

