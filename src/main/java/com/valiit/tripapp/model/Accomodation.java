package com.valiit.tripapp.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;


public class Accomodation {
    private int tripID;
    private Integer id;
    private String accomodationType;
    private String accomodationName;
    private String accomodationAddress;
    private String accomodationPhoneNo;
    private LocalDate accomodationStartTime;
    private LocalDate accomodationEndTime;
    private String accomodationExtraInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public String getAccomodationType() {
        return accomodationType;
    }

    public void setAccomodationType(String accomodationType) {
        this.accomodationType = accomodationType;
    }

    public String getAccomodationName() {
        return accomodationName;
    }

    public void setAccomodationName(String accomodationName) {
        this.accomodationName = accomodationName;
    }

    public String getAccomodationAddress() {
        return accomodationAddress;
    }

    public void setAccomodationAddress(String accomodationAddress) {
        this.accomodationAddress = accomodationAddress;
    }

    public String getAccomodationPhoneNo() {
        return accomodationPhoneNo;
    }

    public void setAccomodationPhoneNo(String accomodationPhoneNo) {
        this.accomodationPhoneNo = accomodationPhoneNo;
    }

    public LocalDate getAccomodationStartTime() {
        return accomodationStartTime;
    }

    public void setAccomodationStartTime(LocalDate accomodationStartTime) {
        this.accomodationStartTime = accomodationStartTime;
    }

    public LocalDate getAccomodationEndTime() {
        return accomodationEndTime;
    }

    public void setAccomodationEndTime(LocalDate accomodationEndTime) {
        this.accomodationEndTime = accomodationEndTime;
    }

    public String getAccomodationExtraInfo() {
        return accomodationExtraInfo;
    }

    public void setAccomodationExtraInfo(String accomodationExtraInfo) {
        this.accomodationExtraInfo = accomodationExtraInfo;
    }
}
