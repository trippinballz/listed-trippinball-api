package com.valiit.tripapp.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Trip {

    private Integer tripID;
    private Integer userID;
    private LocalDate tripStartTime;
    private LocalDate tripEndTime;
    private String tripLocation;
    private String tripType;


    List<Accomodation> accomodations = new ArrayList<>();
    List<Checklist> checklists = new ArrayList<>();
    List<Task> tasks = new ArrayList<>();
    List<Transport> transport = new ArrayList<>();
    List<TravelDocument> travelDocuments = new ArrayList<>();
    List<User> user = new ArrayList<>();


    public Integer getTripID() {
        return tripID;
    }

    public void setTripID(Integer tripID) {
        this.tripID = tripID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public LocalDate getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(LocalDate tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public LocalDate getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(LocalDate tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String getTripLocation() {
        return tripLocation;
    }

    public void setTripLocation(String tripLocation) {
        this.tripLocation = tripLocation;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public List<Accomodation> getAccomodations() {
        return accomodations;
    }

    public void setAccomodations(List<Accomodation> accomodations) {
        this.accomodations = accomodations;
    }

    public List<Checklist> getChecklists() {
        return checklists;
    }

    public void setChecklists(List<Checklist> checklists) {
        this.checklists = checklists;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Transport> getTransport() {
        return transport;
    }

    public void setTransport(List<Transport> transport) {
        this.transport = transport;
    }

    public List<TravelDocument> getTravelDocuments() {
        return travelDocuments;
    }

    public void setTravelDocuments(List<TravelDocument> travelDocuments) {
        this.travelDocuments = travelDocuments;
    }
}

