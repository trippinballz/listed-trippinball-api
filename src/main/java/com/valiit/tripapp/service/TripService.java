package com.valiit.tripapp.service;

import com.valiit.tripapp.dto.TripDto;
import com.valiit.tripapp.model.*;
import com.valiit.tripapp.repository.TripRepository;
import com.valiit.tripapp.repository.UserRepository;
import com.valiit.tripapp.util.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

//testgf
@Service
public class TripService {

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Trip> getTrips() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName(); // siit tuleb nimi
        int userId = userRepository.getUserByUsername(name).getId(); // võtab nime, tagastab objekti ja tagastab id
        return tripRepository.getTrips(userId);
    }



    public Trip getTrip(int tripID) {
        Assert.isTrue(tripID > 0, "The ID of the trip not specified");

        return tripRepository.getTrip(tripID);
    }

    public Accomodation getAccomodation(Integer id) {
        Assert.isTrue(id > 0, "The ID of the accomodation not specified");

        return tripRepository.getAccomodation(id);
    }

    public Transport getTransport(Integer id) {
        Assert.isTrue(id > 0, "The ID of the transport not specified");

        return tripRepository.getTransport(id);
    }

    public TravelDocument getTravelDocument(Integer id) {
        Assert.isTrue(id > 0, "The ID of the travel document not specified");

        return tripRepository.getTravelDocument(id);
    }


    public void saveTrip(Trip trip) {
        Assert.hasText(trip.getTripLocation(), "Trip location not specified");

        String name = SecurityContextHolder.getContext().getAuthentication().getName(); // siit tuleb nimi
        int userId = userRepository.getUserByUsername(name).getId(); // võtab nime, tagastab objekti ja tagastab id
        trip.setUserID(userId);

        if (trip.getTripID() != null && trip.getTripID() > 0) {
            tripRepository.updateTrip(trip);
        } else {
            Assert.isTrue(!tripRepository.tripExists(trip), "The trip with the specified name already exists");
            tripRepository.addTrip(trip);
        }
    }

    public void saveAccomodation(Accomodation accomodation) {

        if (accomodation.getId() != null && accomodation.getId() > 0) {
            tripRepository.updateAccomodation(accomodation);
        } else {
            Assert.isTrue(!tripRepository.accomodationExists(accomodation), "The accomodation with the specified name already exists");
            tripRepository.addAccomodation(accomodation);
        }
    }

    public void saveChecklist(Checklist checklist) {
        if (checklist.getId() != null && checklist.getId() > 0) {
            tripRepository.updateChecklist(checklist);
        } else {
            Assert.isTrue(!tripRepository.checklistExists(checklist), "The checklist already exists");
            tripRepository.addChecklist(checklist);
        }
    }

    public void saveTask(Task task) {
        if (task.getId() != null && task.getId() > 0) {
            tripRepository.updateTask(task);
        } else {
            Assert.isTrue(!tripRepository.taskExists(task), "The task already exists");
            tripRepository.addTask(task);
        }
    }

    public void saveTransport(Transport transport) {
        if (transport.getId() != null && transport.getId() > 0) {
            tripRepository.updateTransport(transport);
        } else {
            Assert.isTrue(!tripRepository.transportExists(transport), "The transport already exists");
            tripRepository.addTransport(transport);
        }
    }

    public void saveTravelDocument(TravelDocument travelDocument) {
        if (travelDocument.getId() != null && travelDocument.getId() > 0) {
            tripRepository.updateTravelDocument(travelDocument);
        } else {
            Assert.isTrue(!tripRepository.travelDocumentExists(travelDocument), "The traveldocument already exists");
            tripRepository.addTravelDocument(travelDocument);
        }
    }





//
//    public void saveTrip(TripDto tripDto) {
//        Assert.notNull(tripDto, "Trip not specified");
//        Assert.hasText(tripDto.getName(), "Trip name not specified");
//        Assert.isTrue(tripDto.getEstablished() == null || tripDto.getEstablished().isBefore(LocalDate.now()),
//                "Trip must have been established in the past");


//        Trip trip = Transformer.toTripModel(tripDto);
//        if (trip.getTripID() != null && trip.getTripID() > 0) {
//            tripRepository.updateTrip(trip);
//        } else {
//            Assert.isTrue(!tripRepository.tripExists(trip), "The trip with the specified name already exists");
//            tripRepository.addTrip(trip);
//        }
//    }
//
    public void deleteTrip(int tripID) {
        if (tripID > 0) {
            tripRepository.deleteTrip(tripID);
        }
    }

    public void deleteAccomodation(Integer id) {
        if (id > 0) {
            tripRepository.deleteAccomodation(id);
        }
    }

    public void deleteChecklistItem(Integer id) {
        if (id > 0) {
            tripRepository.deleteChecklistItem(id);
        }
    }

    public void deleteTask(Integer id) {
        if (id > 0) {
            tripRepository.deleteTask(id);
        }
    }

    public void deleteTransport(Integer id) {
        if (id > 0) {
            tripRepository.deleteTransport(id);
        }
    }

    public void deleteTravelDocument(Integer id) {
        if (id > 0) {
            tripRepository.deleteTravelDocument(id);
        }
    }

}
