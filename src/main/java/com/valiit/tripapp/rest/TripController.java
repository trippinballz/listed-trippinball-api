package com.valiit.tripapp.rest;


import com.valiit.tripapp.dto.TripDto;
import com.valiit.tripapp.model.*;
import com.valiit.tripapp.repository.UserRepository;
import com.valiit.tripapp.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trips")

@CrossOrigin("*")
public class TripController {

    @Autowired
    private TripService tripService;

    @GetMapping
    public List<Trip> getTrips() {
        return tripService.getTrips();
    }


    @GetMapping("/{tripID}")
    public Trip getTrip(@PathVariable("tripID") int tripID) {

        return tripService.getTrip(tripID);
    }

    @GetMapping("/accomodation/{id}")
    public Accomodation getAccomodation(@PathVariable("id") Integer id) {

        return tripService.getAccomodation(id);
    }

    @GetMapping("/transport/{id}")
    public Transport getTransport(@PathVariable("id") Integer id) {

        return tripService.getTransport(id);
    }

    @GetMapping("/travelDocument/{id}")
    public TravelDocument getTravelDocument(@PathVariable("id") Integer id) {

        return tripService.getTravelDocument(id);
    }



    @PostMapping("/accomodation")
    public void saveAccomodation(@RequestBody Accomodation accomodation) {
        tripService.saveAccomodation(accomodation);
    }

    @PostMapping("/checklist")
    public void saveChecklist(@RequestBody Checklist checklist) {
        tripService.saveChecklist(checklist);
    }

    @PostMapping("/task")
    public void saveTask(@RequestBody Task task) {
        tripService.saveTask(task);
    }

    @PostMapping("/transport")
    public void saveTransport(@RequestBody Transport transport) {
        tripService.saveTransport(transport);
    }

    @PostMapping("/travelDocument")
    public void saveTravelDocument(@RequestBody TravelDocument travelDocument) {
        tripService.saveTravelDocument(travelDocument);
    }


    @PostMapping("/trip")
    public void saveTrip(@RequestBody Trip trip) {
        tripService.saveTrip(trip);
    }


    //
    @DeleteMapping("/{tripID}")
    public void deleteTrip(@PathVariable("tripID") int tripID) {
        tripService.deleteTrip(tripID);
    }

    @DeleteMapping("/accomodation/{id}")
    public void deleteAccomodation(@PathVariable("id") Integer id) {
        tripService.deleteAccomodation(id);
    }

    @DeleteMapping("/checklist/{id}")
    public void deleteChecklistItem(@PathVariable("id") Integer id) {
        tripService.deleteChecklistItem(id);
    }

    @DeleteMapping("/task/{id}")
    public void deleteTask(@PathVariable("id") Integer id) {
        tripService.deleteTask(id);
    }

    @DeleteMapping("/transport/{id}")
    public void deleteTransport(@PathVariable("id") Integer id) {
        tripService.deleteTransport(id);
    }

    @DeleteMapping("/travelDocument/{id}")
    public void deleteTravelDocument(@PathVariable("id") Integer id) {
        tripService.deleteTravelDocument(id);
    }
}