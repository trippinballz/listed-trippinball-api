package com.valiit.tripapp.rest;

import com.valiit.tripapp.dto.GenericResponseDto;
import com.valiit.tripapp.dto.JwtRequestDto;
import com.valiit.tripapp.dto.JwtResponseDto;
import com.valiit.tripapp.dto.UserRegistrationDto;
import com.valiit.tripapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}