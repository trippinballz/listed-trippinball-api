package com.valiit.tripapp.dto;

import java.time.LocalDate;

public class TripDto {

    private Integer tripID;
    private Integer userID;
    private LocalDate tripStartTime;
    private LocalDate tripEndTime;
    private String tripLocation;
    private String tripType;

    public Integer getTripID() {
        return tripID;
    }

    public void setTripID(Integer tripID) {
        this.tripID = tripID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public LocalDate getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(LocalDate tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public LocalDate getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(LocalDate tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String getTripLocation() {
        return tripLocation;
    }

    public void setTripLocation(String tripLocation) {
        this.tripLocation = tripLocation;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }
}