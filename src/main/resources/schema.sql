DROP TABLE IF EXISTS `accomodation`;
DROP TABLE IF EXISTS `checklist`;
DROP TABLE IF EXISTS `task`;
DROP TABLE IF EXISTS `transport`;
DROP TABLE IF EXISTS `traveldocuments`;
DROP TABLE IF EXISTS `trip`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`password` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`email` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`id`)
);
CREATE TABLE `trip` (
	`tripID` INT(11) NOT NULL AUTO_INCREMENT,
	`userID` INT(11) NOT NULL DEFAULT 0,
	`tripStartTime` DATE NULL DEFAULT NULL,
	`tripEndTime` DATE NULL DEFAULT NULL,
	`tripLocation` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`tripType` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`tripID`),
	INDEX `userID` (`userID`),
	CONSTRAINT `FK_trip_user` FOREIGN KEY (`userID`) REFERENCES `user` (`id`)
);

CREATE TABLE `accomodation` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tripID` INT(11) NOT NULL,
	`accomodationType` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`accomodationName` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`accomodationAddress` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`accomodationPhoneNo` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`accomodationStartTime` DATETIME NULL DEFAULT NULL,
	`accomodationEndTime` DATETIME NULL DEFAULT NULL,
	`accomodationExtraInfo` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `tripID` (`tripID`),
	CONSTRAINT `FK_accomodation_trip` FOREIGN KEY (`tripID`) REFERENCES `trip` (`tripID`)
);
CREATE TABLE `checklist` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tripID` INT(11) NOT NULL,
	`listItem` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `tripID` (`tripID`),
	CONSTRAINT `FK_list_trip` FOREIGN KEY (`tripID`) REFERENCES `trip` (`tripID`)
);

CREATE TABLE `task` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tripID` INT(11) NOT NULL,
	`taskItem` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_task_trip` (`tripID`),
	CONSTRAINT `FK_task_trip` FOREIGN KEY (`tripID`) REFERENCES `trip` (`tripID`)
);

CREATE TABLE `transport` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tripID` INT(11) NOT NULL,
	`transportType` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`transportProvider` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`transportStartTime` DATETIME NULL DEFAULT NULL,
	`transportEndTime` DATETIME NULL DEFAULT NULL,
	`transportStartLocation` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`transportEndLocation` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_transport_trip` (`tripID`),
	CONSTRAINT `FK_transport_trip` FOREIGN KEY (`tripID`) REFERENCES `trip` (`tripID`)
);

CREATE TABLE `traveldocuments` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tripID` INT(11) NOT NULL DEFAULT 0,
	`travelDocumentName` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`travelDocumentStartTime` DATE NULL DEFAULT NULL,
	`travelDocumentEndTime` DATE NULL DEFAULT NULL,
	`travelDocumentExtraInfo` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `tripID` (`tripID`),
	CONSTRAINT `FK_traveldocuments_trip` FOREIGN KEY (`tripID`) REFERENCES `trip` (`tripID`)
);

