
INSERT INTO `user` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

INSERT INTO `user` (`id`, `username`, `password`, `email`) VALUES
	(3, 'kakuke', 'parool11', 'kakuke@hot.ee'),
	(4, 'märten', 'märten', 'user@hotmail.com'),
	(5, 'kleiri', 'kleiri', 'kleiri@kleiri.com');

INSERT INTO `trip` (`tripID`, `userID`, `tripStartTime`, `tripEndTime`, `tripLocation`, `tripType`) VALUES
	(2, 5, '2019-11-01', '2019-11-25', 'Argentiina', 'Business'),
	(3, 5, '2018-11-21', '2019-08-31', 'Riia', 'Business'),
	(4, 4, '2010-12-01', '2011-02-21', 'Võru', 'Business'),
	(5, 3, '2017-11-21', '2019-05-21', 'Nepaal', 'Vacation');

INSERT INTO `accomodation` (`id`, `tripID`, `accomodationType`, `accomodationName`, `accomodationAddress`, `accomodationPhoneNo`, `accomodationStartTime`, `accomodationEndTime`, `accomodationExtraInfo`) VALUES
	(1, 4, 'automajutus', 'Automajutus OÜ', 'Tatra 23', '+568871348', '2019-11-21 14:17:50', '2019-11-21 14:17:52', 'Peab kaasa võtma oma teki'),
	(2, 5, 'onn', 'mägi onn oü', 'kolmas kurv', '+56824557', '2019-11-21 14:19:27', '2019-11-21 14:19:28', 'kohale saab ainult eesli seljas');

INSERT INTO `checklist` (`id`, `tripID`, `listItem`) VALUES
	(1, 5, 'osta taskurätt'),
	(2, 2, 'kasta lilled'),
	(3, 3, 'vii vanaema vanadekodusse'),
	(4, 4, 'vii taara ära');

INSERT INTO `task` (`id`, `tripID`, `taskItem`) VALUES
	(1, 5, 'taskurätt'),
	(2, 5, 'trussikud'),
	(3, 5, 'särk'),
	(4, 2, 'vesi'),
	(5, 2, 'mahl'),
	(6, 4, 'läpakas'),
	(7, 2, 'rätik');

INSERT INTO `transport` (`id`, `tripID`, `transportType`, `transportProvider`, `transportStartTime`, `transportEndTime`, `transportStartLocation`, `transportEndLocation`) VALUES
	(1, 2, 'Lennuk', 'Malaysian', '2019-11-21 14:13:41', '2019-11-21 16:06:44', 'Tallinna Lennujaam', 'Argentina Lennujaam'),
	(2, 4, 'Auto rent', 'Rendiauto UÜ', '2019-10-12 12:14:22', '2019-12-08 14:14:35', 'tallinna autorent', 'Võru Pagar');

INSERT INTO `traveldocuments` (`id`, `tripID`, `travelDocumentName`, `travelDocumentStartTime`, `travelDocumentEndTime`, `travelDocumentExtraInfo`) VALUES
	(1, 5, 'Viisa', '2017-11-21', '2019-05-21', 'Piiril vaja maksta 25€'),
	(2, 4, 'Reisikindlustus', '2010-12-21', '2011-02-21', NULL);



--INSERT INTO `company` VALUES
--    (1,'Arko Vara','https://www.nasdaqbaltic.com/market/logo.php?issuer=ARC','1994-07-04',20,3640000.00,-540000.00,8998367,1.09,0.01),
--    (2,'Baltika','https://www.nasdaqbaltic.com/market/logo.php?issuer=BLT','1997-05-09',946,44690000.00,-5120000.00,4079485,0.32,NULL),
--    (3,'Ekspress Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=EEG','1995-06-21',1698,60490000.00,10000.00,29796841,0.84,NULL),
--    (4,'Harju Elekter','https://www.nasdaqbaltic.com/market/logo.php?issuer=HAE','1996-05-14',744,120800000.00,1550000.00,17739880,4.25,0.18),
--    (5,'LHV Group','https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV','2005-01-25',366,64540000.00,25240000.00,26016485,11.75,0.21),
--    (6,'Merko Ehitus','https://www.nasdaqbaltic.com/market/logo.php?issuer=MRK','1990-11-05',740,418010000.00,19340000.00,17700000,9.30,1.00),
--    (7,'Nordecon','https://www.nasdaqbaltic.com/market/logo.php?issuer=NCN','1998-01-01',662,223500000.00,3380000.00,32375483,1.04,0.12),
--    (8,'Pro Kapital Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=PKG','1994-01-01',91,27990000.00,16830000.00,56687954,1.43,NULL),
--    (9,'Tallink Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=TAL','1997-08-21',7201,949720000.00,40050000.00,669882040,0.96,0.12),
--    (10,'Tallinna Kaubamaja Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=TKM','1960-07-21',4293,681180000.00,30440000.00,40729200,8.36,0.71),
--    (11,'Tallinna Sadam','https://www.nasdaqbaltic.com/market/logo.php?issuer=TSM','1991-12-25',468,130640000.00,24420000.00,263000000,1.96,0.13),
--    (12,'Tallinna Vesi','https://www.nasdaqbaltic.com/market/logo.php?issuer=TVE','1967-01-01',314,62780000.00,24150000.00,20000000,10.80,0.75);



